package sudoku;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.concurrent.TimeUnit;
import javax.swing.*;

public class SudokuGUI extends JFrame{
    private JToggleButton[][] toggleButtons1;
    private int time;
    private JFrame frame = this;
    
    public SudokuGUI() {
        super("Sudoku");
        setSize(550,520);
        setResizable(false);
        setLookAndFeel();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        toggleButtons1 = new JToggleButton[9][9];
        ButtonGroup toggleButtons1Group = new ButtonGroup();
        JPanel panel = new JPanel();
        JPanel[][] panel1 = new JPanel[3][3];
        GridLayout gridlayout1 = new GridLayout(3,3,5,5);
        panel.setLayout(gridlayout1);
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                panel1[i][j] = new JPanel();
                panel1[i][j].setLayout(gridlayout1);
                panel1[i][j].setBorder(BorderFactory.createEtchedBorder());
                panel.add(panel1[i][j]);
            }
        }
        
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                toggleButtons1[i][j] = new JToggleButton();
                toggleButtons1[i][j].setFont(new Font("Arial", Font.BOLD, 18));
                toggleButtons1[i][j].setForeground(Color.GREEN);
                toggleButtons1[i][j].setActionCommand("toggleButtons1");
                panel1[i/3][j/3].add(toggleButtons1[i][j]);
                toggleButtons1Group.add(toggleButtons1[i][j]);
            }
        }
        
        //completarea tabelului aleator cu una din cele 8 grile aflate in fisiere
        new NewSudokuFromFile("sudoku" + File.separator + "sudoku" + (int)(Math.random()*1 + 1) + ".txt");
        
        BorderLayout borderLayout = new BorderLayout();
        setLayout(borderLayout);
        add(panel,BorderLayout.CENTER);
        
        JButton start = new JButton("GENEREAZA SOLUTIA");
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                start.setEnabled(false);
                while (true) {
                    String s = JOptionPane.showInputDialog(frame,
                            "Introduceti timpul de intarziere a solutiei in milisecunde",
                            "Time", JOptionPane.PLAIN_MESSAGE);
                    try {
                        time = Integer.parseInt(s);
                        break;
                    } catch (Exception exc) {
                        System.out.println(exc);
                    }
                }
                
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Solution s = new Solution();
                        s.startSolution();
                    }
                });
                t.start();
            }
            
        });
        start.setFont(new Font("Arial", Font.BOLD, 18));
        
        add(start, BorderLayout.SOUTH);
        
        setVisible(true);
    }
    
    private void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(
                "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"
            );
        } catch (Exception exc) {
            // ignore error
        }
    }
    
    private class NewSudokuFromFile {
        private NewSudokuFromFile(String filename) {
            int i = 0, j = 0;
            try {
                File file = new File(filename);
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line = br.readLine();

                while (line != null) {
                    for (char ch : line.toCharArray()) {
                        if (ch != ',') {
                            if (ch == '0') {
                                toggleButtons1[i][j].setText(" ");
                                toggleButtons1[i][j].setEnabled(true);
                            } else {
                                toggleButtons1[i][j].setText(ch + "");
                                toggleButtons1[i][j].setEnabled(false);
                            }
                            j++;
                            if (j >= 9) {
                                i++;
                                j = 0;
                            }
                        }
                    }
                    line = br.readLine();
                }

            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
    
    private class Solution {
        public void startSolution() {
            /*temp[i][j]=
                - 0, daca valoarea de pe linia i si coloana j poate fi modificata
                - 1, daca valoarea de pe linia i si coloana j nu poate fi modificata si s-a efectuat un numar
                    par de treceri de aceasta pozitie
                - 2, daca valoarea de pe linia i si coloana j nu poate fi modificata si s-a efectuat un numar
                    impar de treceri de aceasta pozitie
            */
            int[][] temp = new int[9][9];
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (toggleButtons1[i][j].isEnabled()) {
                        temp[i][j] = 0; //valoarea poate fi modoficata
                        toggleButtons1[i][j].setForeground(Color.BLUE);
                    } else {
                        temp[i][j] = 1; // este o valoare fixata
                    }
                }
            }
            
            int i = 0, j = 0;
            
            while ( (0 <= i) && (i <= 8) ) {
                while ( (0 <= j) && (j <= 8) ) {
                    switch (temp[i][j]) {
                        case 0://valoarea poate fi modificata
                            int ok = 0;
                            if (toggleButtons1[i][j].getText().equals(" ")) {
                                //valoarea nu a fost modificata niciodata sau nici una din valori
                                //nu a indeplinit conditiile
                                ok = 1;
                                for (int k = 1; k <=9; k++) {
                                    setColors();
                                    toggleButtons1[i][j].setText(k + "");
                                    //timeDelay(time);
                                    if ( (lineVerify(i,j,k)) && (squareVerify(i,j,k)) ) {
                                        ok = 2;
                                        j++;
                                        timeDelay(time);
                                        break;//putem trece la urmatoarea pozitie in tablou
                                    }
                                    timeDelay(time);
                                }
                            } else {//trebuie sa modificam o val pusa anterior
                                if ( toggleButtons1[i][j].getText().equals("9") || (ok == 1) ){
                                    setColors();
                                    toggleButtons1[i][j].setText(" ");
                                    timeDelay(time);
                                    j--;//ne int la elem anterior (nu mai avem alte val posibile)
                                } else {
                                    int x = Integer.parseInt(toggleButtons1[i][j].getText());
                                    for (int k = x+1; k <=9; k++) {
                                        setColors();
                                        toggleButtons1[i][j].setText(k + "");//luam urmatoarea valoare posibila
                                        //timeDelay(time);
                                        if ( (lineVerify(i,j,k)) && (squareVerify(i,j,k)) ) {
                                            j++;
                                            timeDelay(time);
                                            break;
                                        } else {
                                            //nici una din valori nu este convenabila
                                        }
                                        timeDelay(time);
                                    }
                                }
                            }
                            break;
                        case 1://valoarea nu poate fi modificata si se trece la urmatorul element
                            temp[i][j] = 2;
                            j++;
                            break;
                        case 2://valoarea nu poate fi modificata si se trece la elementul anterior
                            temp[i][j] = 1;
                            j--;
                    }
                }
                
                if (j < 0) {
                    j = 8;
                    i--;
                }
                if (j > 8) {
                    j = 0;
                    i++;
                }
            }
            if (i < 0) {
                JOptionPane.showMessageDialog(frame, "Aceasta varianta nu are solutia",
                        "Sfarsit", JOptionPane.PLAIN_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(frame, "Aceasta este solutia acestei variante",
                        "Sfarsit", JOptionPane.PLAIN_MESSAGE);
            }
        }
        
        private boolean lineVerify(int i, int j, int val) {
            for (int count = 0; count < 9; count++) {
                if ( (toggleButtons1[count][j].getText().equals(val + "")) &&
                        (count != i) ){
                    //daca gasim pe aceeasi coloana o valoare identica
                    toggleButtons1[i][j].setBackground(Color.RED);
                    toggleButtons1[count][j].setBackground(Color.RED);
                    return false;
                }
                if ( (toggleButtons1[i][count].getText().equals(val + "")) &&
                        (count != j) ){
                    //daca gasim pe aceeasi linie o valoare identica
                    toggleButtons1[i][j].setBackground(Color.RED);
                    //toggleButtons1[i][count].setBackground(Color.RED);
                    return false;
                }
            }
            return true;//nici una din valorile de pe linia i sau de pe coloana j nu corespune cu val
        }
        
        private boolean squareVerify(int i, int j, int val) {
            for ( int k = i - (i%3); k <= (i - (i%3) + 2); k++ ) {
                for ( int l = j - (j%3); l <= (j - (j%3) + 2); l++ ) {
                    if ( ( (k != i) || (l != j) ) && 
                            (toggleButtons1[k][l].getText().equals(val + "") ) ) {
                        toggleButtons1[i][j].setBackground(Color.RED);
                        //toggleButtons1[k][l].setBackground(Color.RED);
                        return false;
                    }
                }
            }
            return true;
        }
        
        private void timeDelay(int x) {
            try {
                TimeUnit.MILLISECONDS.sleep(x);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        
        private void setColors() {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (toggleButtons1[i][j].isEnabled()) {
                        toggleButtons1[i][j].setBackground(Color.GRAY);
                    } else {
                        toggleButtons1[i][j].setBackground(Color.GRAY);
                    }
                }
            }
        }
    }
}
